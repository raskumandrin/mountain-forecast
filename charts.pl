#!/usr/bin/perl

use strict;

use FindBin;
use lib "$FindBin::Bin/lib";

use Mojo::DOM;
use Mojo::Util qw(slurp);

use List::Util qw(max min sum);
use utf8;
binmode(STDOUT,':utf8');

=begin
Max Wind (km/h)
Sum Snow (cm),
High °C,
Low °C,
Min Chill °C
=end
=cut

my %charts = (
    'max_wind'  => 'Max Wind (km/h)',
    'sum_snow'  => 'Snow (cm)',
    'high_temp' => 'High °C',
    'low_temp'  => 'Low °C',
    'min_chill' => 'Min Chill °C',
);

my %coloraxis = (
    'max_wind'  => "colors: ['#FFFFFF','#0000FF']",
    'sum_snow'  => "colors: ['#FFFFFF','#0000FF']",
    'high_temp' => "values: [-40, 0, 20], colors: ['#0080FF','#ffffff','#FC6E00']",
    'low_temp'  => "values: [-40, 0, 20], colors: ['#0080FF','#ffffff','#FC6E00']",
    'min_chill' => "values: [-40, 0, 10], colors: ['#0080FF','#ffffff','#FC6E00']",
);

foreach my $dir (qw(1758 2488 3380 3437 3706 4058)) {
    open my $fh,'<:utf8',"data/$dir.csv";
    
    my ( %max_wind, %sum_snow, %high_temp, %low_temp, %min_chill);
    
    while (my $line = <$fh>) {
        chomp($line);
        next unless $line =~ /^\d+/; # check that is data line (no header)
        my @a = split /\t/,$line;
        
        $max_wind{ $a[0] } = max( $a[1],$a[2],$a[3]);
        $sum_snow{ $a[0] } = sum( $a[4],$a[5],$a[6]);
        $high_temp{ $a[0] } = max( $a[7],$a[8],$a[9]);
        $low_temp{ $a[0] } = min( $a[10],$a[11],$a[12]);
        $min_chill{ $a[0] } = min( $a[13],$a[14],$a[15]);
        
    }
    close $fh;

    sub generate_chart_html {
        my ($dir) = ($_[0]);
        my ($type) = ($_[1]);
        my ($hash) = ($_[2]);

        my @s;
        foreach my $date ( keys %$hash ) {
            my ($d,$m,$y) = ( $date =~ /(\d{2})\.(\d{2})\.(\d{4})/ );
            push @s,sprintf('[ new Date(%d, %d, %d), %d ]',$y,$m-1,$d,$hash->{$date});
        }
        my $data = join ",\n",@s;
        
        open my $fh,'>:utf8',"data/$dir-$type.html";
        print $fh <<"EOF";
        <html>
  <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["calendar"]});
      google.charts.setOnLoadCallback(drawChart);

   function drawChart() {
       var dataTable = new google.visualization.DataTable();
       dataTable.addColumn({ type: 'date', id: 'Date' });
       dataTable.addColumn({ type: 'number', id: 'Won/Loss' });
       dataTable.addRows([
       $data
        ]);

       var chart = new google.visualization.Calendar(document.getElementById('calendar_basic'));

       var options = {
         title: "$charts{$type}",
         calendar: { cellSize: 12 },
         colorAxis: { $coloraxis{$type} },
         height: 240,
       };

       chart.draw(dataTable, options);
   }
    </script>
  </head>
  <body>
    <div id="calendar_basic" style="width: 700px; height: 240px;"></div>
  </body>
</html>
EOF

    close $fh;
    
    }

    generate_chart_html($dir,'max_wind',\%max_wind);
    generate_chart_html($dir,'sum_snow',\%sum_snow);
    generate_chart_html($dir,'high_temp',\%high_temp);
    generate_chart_html($dir,'low_temp',\%low_temp);
    generate_chart_html($dir,'min_chill',\%min_chill);

}