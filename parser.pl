#!/usr/bin/perl

use strict;

use FindBin;
use lib "$FindBin::Bin/lib";

use Mojo::DOM;
use Mojo::Util qw(slurp);

binmode(STDOUT,':utf8');

foreach my $dir (qw(1758 2488 3380 3437 3706 4058)) {
    open my $fh,'>:utf8',"data/$dir.csv";
    
    print $fh join("\t",
        'Date'
        ,'Wind AM (km/h)'       , 'Wind PM (km/h)'       , 'Wind night (km/h)'
        ,'Snow AM (cm)'         , 'Snow PM (cm)'         , 'Snow night (cm)'
        ,'High °C AM'           , 'High °C PM'           , 'High °C night'
        ,'Low °C AM'            , 'Low °C PM'            , 'Low °C night'
        ,'Chill °C AM'          , 'Chill °C PM'          , 'Chill °C night',
        ,'Freezing level AM (m)', 'Freezing level PM (m)', 'Freezing level night (m)'
        ,'Summary')."\n";
    
    opendir(my $dh, "$FindBin::Bin/$dir") || die "Can't open dir: $!\n";
        while(readdir $dh) {
            my %d;
            ($d{year},$d{month},$d{day}) = ( /(\d{4})-(\d{2})-(\d{2})/ );

            if ($d{year} and $d{month} and $d{day}) {
                
                my $dom = Mojo::DOM->new( slurp("$FindBin::Bin/$dir/$_") );
                
# phrase
                if ( $dom->find('span.phrase')->size() ) {
                    $d{phrase} = $dom->find('span.phrase')->first->text;
                }

# wind
                if ( $dom->find('span.wind')->size() ) {
                    $d{wind_am} = $dom->find('span.wind')->first->text;
                }
                if ( $dom->find('span.wind')->size() > 1 ) {
                    $d{wind_pm} = $dom->find('span.wind')->[1]->text;
                }
                if ( $dom->find('span.wind')->size() > 2 ) {
                    $d{wind_nt} = $dom->find('span.wind')->[2]->text;
                }
# snow                
                if ( $dom->find('span.snow')->size() ) {
                    $d{snow_am} = $dom->find('span.snow')->first->text;
                }
                if ( $dom->find('span.snow')->size() > 1 ) {
                    $d{snow_pm} = $dom->find('span.snow')->[1]->text;
                }
                if ( $dom->find('span.snow')->size() > 2 ) {
                    $d{snow_nt} = $dom->find('span.snow')->[2]->text;
                }

# temp high
                if ( $dom->find('span.temp')->size() ) {
                    $d{temp_hi_am} = $dom->find('span.temp')->first->text;
                }
                if ( $dom->find('span.temp')->size() > 1 ) {
                    $d{temp_hi_pm} = $dom->find('span.temp')->[1]->text;
                }
                if ( $dom->find('span.temp')->size() > 2 ) {
                    $d{temp_hi_nt} = $dom->find('span.temp')->[2]->text;
                }
# temp low
                if ( $dom->find('span.temp')->size() > 18 ) {
                    $d{temp_lo_am} = $dom->find('span.temp')->[18]->text;
                }
                if ( $dom->find('span.temp')->size() > 19 ) {
                    $d{temp_lo_pm} = $dom->find('span.temp')->[19]->text;
                }
                if ( $dom->find('span.temp')->size() > 20 ) {
                    $d{temp_lo_nt} = $dom->find('span.temp')->[20]->text;
                }
# temp chill
                if ( $dom->find('span.temp')->size() > 36 ) {
                    $d{temp_ch_am} = $dom->find('span.temp')->[36]->text;
                }
                if ( $dom->find('span.temp')->size() > 37 ) {
                    $d{temp_ch_pm} = $dom->find('span.temp')->[37]->text;
                }
                if ( $dom->find('span.temp')->size() > 38 ) {
                    $d{temp_ch_nt} = $dom->find('span.temp')->[38]->text;
                }            
# heightfl                
                if ( $dom->find('span.heightfl')->size() ) {
                    $d{heightfl_am} = $dom->find('span.heightfl')->first->text;
                }
                if ( $dom->find('span.heightfl')->size() > 1 ) {
                    $d{heightfl_pm} = $dom->find('span.heightfl')->[1]->text;
                }
                if ( $dom->find('span.heightfl')->size() > 2 ) {
                    $d{heightfl_nt} = $dom->find('span.heightfl')->[2]->text;
                }
                
$d{snow_am} = 0 if $d{snow_am} eq '-';
$d{snow_pm} = 0 if $d{snow_pm} eq '-';
$d{snow_nt} = 0 if $d{snow_nt} eq '-';
                   

print $fh join("\t",
    "$d{day}.$d{month}.$d{year}"
    ,$d{wind_am},$d{wind_pm},$d{wind_nt}
    ,$d{snow_am},$d{snow_pm},$d{wind_nt}
    ,$d{temp_hi_am},$d{temp_hi_pm},$d{temp_hi_nt}
    ,$d{temp_lo_am},$d{temp_lo_pm},$d{temp_lo_nt}
    ,$d{temp_ch_am},$d{temp_ch_pm},$d{temp_ch_nt}
    ,$d{heightfl_am},$d{heightfl_pm},$d{heightfl_nt}
    ,$d{phrase})."\n";

#                print $fh <<"EOF";
#$d{day}.$d{month}.$d{year}\t
#    wind: $d{wind_am} $d{wind_pm} $d{wind_nt}
#    snow: $d{snow_am} $d{snow_pm} $d{snow_nt}
#    temp high: $d{temp_hi_am} $d{temp_hi_pm} $d{temp_hi_nt}
#    temp low: $d{temp_lo_am} $d{temp_lo_pm} $d{temp_lo_nt}
#    temp chill: $d{temp_ch_am} $d{temp_ch_pm} $d{temp_ch_nt}
#    heightfl: $d{heightfl_am} $d{heightfl_pm} $d{heightfl_nt}
#    phrase: $d{phrase}
#EOF
           #     exit;
        }
    }
    closedir $dh;
    close $fh;
}